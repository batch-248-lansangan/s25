console.log("Hello World!");

/*	
	JSON
		JSON stands for JavaScript Object Notation
		JSON is also used in other programming languages

		Syntax:
		{
			"propertyA" : "valueA",
			"propertyB" : "valueB"
		};

		JSON are wrapped in curly braces
		Properties/keys are wrapped in double quotations


*/

		let sample1 = `
			{
				"name" : "Cardo Dalisay",	
				"age" : 20,
				"address" : {
					"city" : "Quezon City",
					"country" : "Philippines"
				}	
			}
			`;
		
		console.log(sample1);


		let person = `
		{
				"name" : "Saul Goodman",
				"occupation" : "Attorney-at-Law",
				"address" : {
					"city" : "Albaquerque",
					"state" : "New Mexico"
 				}
 		}

		`;

		console.log(person);

		let sample2 = `
			{
				"name" : "Win Lansangan",	
				"age" : 20,
				"address" : {
					"city" : "Angeles City",
					"country" : "Philippines"
				}	
			}
			`;
		
		console.log(sample2);
		console.log(typeof sample1);

		//Are we able to return JSON into a JS Object?
		//JSON.parse() - will return JSON as an object

		console.log(JSON.parse(sample1));
		console.log(JSON.parse(sample2));

		//JSON Array
		//JSON Array is an array of JSON

		let sampleArr = `

			[
				{
					"email" : "sinayangmoko@gmail.com",	
					"password" : "february14",
					"isAdmin" : false 
				},
				{
					"email":"dalisay@cardo.com",
					"password":"thecountryman",
					"isAdmin":"false"
				},
				{
					"email":"jsonv@gmail.com",
					"password":"friday13",
					"isAdmin":"true"
				}

			]

		`;

		console.log(sampleArr);

		//Can we use Array methods on a JSON Array?
		//No. Because a JSON is a sting

		//so what can we do to be able to add more items/objects into our sampleArr?

		//PARSE the JSON to a JS array and save it as a variable

		let parsedSampleArr = JSON.parse(sampleArr);
		console.log(parsedSampleArr);
		console.log(typeof parsedSampleArr);//object - array

		//can we now delete the last item in te JSON Array?

		console.log(parsedSampleArr.pop());
		console.log(parsedSampleArr);

		//if for example we need to send this data back to our client/front endit should be in JSON format

		//JSON.parse() does not mutate or update the original JSON
		//we can actually turn a JS object into a JSON
		//JSON.stringify() - this will stringify JS objects as JSON

		sampleArr = JSON.stringify(parsedSampleArr);
		console.log(sampleArr);

		//Database (JSON) => Server/API (JSON to JS Object to process) => sent as JSON = > frontend/client

		let jsonArr = `

			[
				"pizza",
				"hamburger",
				"spaghetti",
				"shanghai",
				"hotdog stick on a pineapple",
				"pancit bihon"



			]


		`;
		console.log(jsonArr);

		let parsedJsonArr = JSON.parse(jsonArr);
		console.log(parsedJsonArr);
	
		parsedJsonArr.pop();
		parsedJsonArr.push("barbecue");

		jsonArr = JSON.stringify(parsedJsonArr);
		console.log(jsonArr);

		//can we now delete the last item in te JSON Array?

	/*	console.log(parsedJsonArr.pop());
		console.log(parsedJsonArr);

		let parsedJsonArrLength = parsedJsonArr.push("barbecue");
		console.log(parsedJsonArr);

		sampleJsonArr = JSON.stringify(parsedJsonArr);
		console.log(sampleJsonArr);

		let parsedSampleJsonArr = JSON.parse(sampleJsonArr);
		console.log(parsedSampleJsonArr); 

*/

		//Gather User Details

		let firstName = prompt("What is your first name?");
		let lastName = prompt("What is your last name?");
		let age = prompt("What is your age?");
		let address = {
			city: prompt("Which city do you live in?"),
			country : prompt("Which country does your city address belong to?")
		};

		let otherData = JSON.stringify({

			firstName: firstName,
			lastName: lastName,
			age:age,
			address:address
		});

		console.log(otherData);

		